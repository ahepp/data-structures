package class2;

/**
 * An interface for Our Array Spec with a generic.
 * @param <T>
 */
public interface DSArray<T> {

    /**
     * Utility method for finding the current length of the Array.
     * @return length of the array
     */
    int length();


    /** Get the value in the array at a particular index.
     @param index the position of the value requested;
     pre-condition: index >= 0 and index < length()
     @return the value at that position
     @throws IndexException if index is bad
     */
    T get(int index) throws IndexException;

    /** Put a value into the array at a particular position.
     @param index the position it goes;  pre-condition:
     index >= 0 and index < length()
     @param in the value being saved
     @throws IndexException if index is bad
     */
    void put(int index, T in) throws IndexException;


}
