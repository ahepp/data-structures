package class2;

public class SimpleArray<T> implements DSArray<T> {

    private T[] data;

    /** Constructor takes a length and a starting value.
     @param len the capacity of the array
     @param val the starting value
     */
    public SimpleArray(int len, T val) {
        //data = new T[len];
        data = (T[]) new Object[len];
        // this generates an allowed compiler warning
        for (int i = 0; i < len; i++) {
            data[i] = val;
        }
    }

    /** Get the declared capacity of the array.
     @return the length
     */
    public int length() {
        return data.length;
    }

    /** Get the value in the array at a particular index.
     @param index the position of the value requested
     pre-conditions: index >= 0 and index < length()
     @return the value at that position
     */
    public T get(int index) {
        if (index < 0) {
            throw new IndexException();
        }
        if (index >= data.length) {
            throw new LengthException();
        }
        return data[index];
    }

    /** Put a value into the array at a particular position.
     @param index the position it goes
     @param t the value being saved
     pre-conditions: index >= 0 and index < length()
     */
    public void put(int index, T t) {
        if (index < 0) {
            throw new IndexException();
        }
        if (index >= data.length) {
            throw new LengthException();
        }
        data[index] = t;
    }

    public static void main(String[] args) {
        DSArray<String> array = new SimpleArray<>(5, "default");
        array.put(2, "This is going in slot 2");
        array.put(3, "This is going in slot 3");

        assert (array.get(0).equalsIgnoreCase("default"));
        assert (array.get(2).equalsIgnoreCase("This is going in slot 2"));
        assert (array.get(3).equalsIgnoreCase("This is going in slot 3"));

        System.out.println("Passed all of our Asserts!");
        //-1 , 7, new -1
    }
}
