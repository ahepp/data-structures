package practice;

public interface Variable<T> {

    T get();

    void put(T input);

}
