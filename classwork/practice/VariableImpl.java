package practice;

public final class VariableImpl<T> implements Variable<T> {

    private T innerValue;

    private VariableImpl(T initial) {
        innerValue = initial;
    }

    public T get() {
        return innerValue;
    }

    public void put(T input) {
        innerValue = input;
    }

    public static void main(String[] args) {
        Variable<String> v = new VariableImpl<String>("Hi");
        System.out.println(v.get());
        v.put("hello");
        System.out.println(v.get());
    }
}
