/* InsertionSort.java */

package hw3;

import hw2.Array;


/**
 * The Insertion Sort algorithm.
 * @param <T> Element type.
 */
public final class InsertionSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {

    //helper function to see if one argument is greater than another.
    private boolean greater(T a, T b) {
        return a.compareTo(b) > 0;
    }

    @Override
    public void sort(Array<T> array) {
        //store length here so we dont have to redo every iteration
        int len = array.length();
        //basic insertion sort implementation
        for (int i = 0; i < len; i++) {
            for (int j = i; j > 0; j--) {
                //store values here to reduce accesses
                T t = array.get(j);
                T y = array.get(j - 1);
                //break if in proper place, otherwise swap
                if (this.greater(t, y)) {
                    break;
                }
                array.put(j, y);
                array.put(j - 1, t);
            }
        }
    }

    @Override
    public String name() {
        return "Insertion Sort";
    }
}
