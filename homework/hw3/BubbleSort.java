/* BubbleSort.java */

package hw3;

import hw2.Array;

/**
 * The Bubble Sort algorithm with the optimized "quick" break to exit
 * if the array is sorted.
 * @param <T> The type being sorted.
 */
public final class BubbleSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {

    //helper method to see if one argument is greater than another
    private boolean greater(T a, T b) {
        return a.compareTo(b) > 0;
    }

    @Override
    public void sort(Array<T> array) {
        //store a variable for length so we dont have to do this every time
        int len = array.length();
        //condition to break early if already sorted
        boolean sorted;
        //helpful conditions to break early if beginning is already sorted
        int last = len - 1;
        int current;
        //basic bubble sort implementation with early exit conditions to
        // optimize
        for (int i = 0; i < len; i++) {
            sorted = true;
            current = -1;
            for (int count = 0; count < last; count++) {
                T t = array.get(count);
                T y = array.get(count + 1);
                if (this.greater(t, y)) {
                    array.put(count, y);
                    array.put(count + 1, t);
                    sorted = false;
                    current = count;
                }
            }
            //break early if already sorted
            if (sorted) {
                return;
            }
            //skip first stuff if already sorted
            last = current;
        }
    }

    @Override
    public String name() {
        return "Bubble Sort";
    }
}
