/* MeasuredArray.java */

package hw3;

import exceptions.IndexException;
import hw2.SimpleArray;

/**
 * An Array that is able to report the number of accesses and mutations,
 * as well as reset those statistics.
 * @param <T> The type of the array.
 */
public class MeasuredArray<T> extends SimpleArray<T> implements Measured<T> {

    private int mutations;
    private int accesses;

    /**
     * Constructor for a MeasuredArray that calls the SimpleArray constructor.
     * @param n The size of the array.
     * @param t The initial value to set every object to in the array..
     */
    public MeasuredArray(int n, T t) {
        super(n, t);
        this.mutations = 0;
        this.accesses = 0;
    }

    @Override
    public int length() {
        accesses++;
        return super.length();
    }

    @Override
    public T get(int i) throws IndexException {
        T val = super.get(i);
        this.accesses++;
        return val;
    }

    @Override
    public void put(int i, T t) throws IndexException {
        super.put(i, t);
        this.mutations++;
    }

    @Override
    public void reset() {
        this.mutations = 0;
        this.accesses = 0;
    }

    @Override
    public int accesses() {
        return this.accesses;
    }

    @Override
    public int mutations() {
        return this.mutations;
    }

    @Override
    public int count(T t) {
        int count = 0;
        int len = this.length();
        for (int i = 0; i < len; i++) {
            if (this.get(i) == t) {
                count++;
            }
        }
        return count;
    }
}
