/* MeasuredArrayTest.java */


package hw3;

import exceptions.IndexException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MeasuredArrayTest {

    private static final int SIZE = 20;
    private static final String VAL = "test";

    private MeasuredArray<String> array;

    @Before
    public void createArray() {
        this.array = new MeasuredArray<>(SIZE, VAL);
    }

    @Test
    public void newArrayZeroMutations() {
        assertEquals(0, array.mutations());
    }

    @Test
    public void newArrayZeroAccesses() {
        assertEquals(0, array.accesses());
    }

    @Test
    public void getWorks() {
        assertEquals("test", array.get(0));
    }

    @Test
    public void putWorks() {
        array.put(0, "hi");
        assertEquals("hi", array.get(0));
    }

    @Test
    public void countWorks() {
        assertEquals(20, array.count("test"));
    }

    @Test
    public void mutationsTest() {
        assertEquals(0, array.mutations());
        array.put(1, "t");
        assertEquals(1, array.mutations());
        array.put(0, "A");
        assertEquals(2, array.mutations());
        array.put(4, "p");
        assertEquals(3, array.mutations());
        //test exceptions are properly thrown
        try {
            array.put(25, "nope");
        }
        catch (IndexException e) {
            //this is fine
        }
        assertEquals(3, array.mutations());
        array.reset();
        assertEquals(0, array.mutations());
        for (int i = 0; i < 15; i++) {
            array.put(i, Integer.toString(i));
        }
        assertEquals(15, array.mutations());
    }

    @Test
    public void accessesTest() {
        assertEquals(0, array.accesses());
        array.length();
        assertEquals(1, array.accesses());
        array.get(0);
        assertEquals(2, array.accesses());
        array.get(4);
        assertEquals(3, array.accesses());
        //test exceptions are properly thrown
        try {
            array.get(25);
        }
        catch (IndexException e) {
            //this is fine
        }
        assertEquals(3, array.accesses());
        array.reset();
        assertEquals(0, array.accesses());
        for (int i = 0; i < 15; i++) {
            array.get(i);
        }
        assertEquals(15, array.accesses());
    }

    @Test
    public void countTest() {
        assertEquals(0, array.count("the"));
        assertEquals(SIZE, array.count(VAL));
        for (int i = 0; i < 5; i++) {
            array.put(i, "hi");
        }
        assertEquals(5, array.count("hi"));
        assertEquals(SIZE - 5, array.count(VAL));
        assertEquals((SIZE + 1) * 4, array.accesses());
        assertEquals(5, array.mutations());
    }

    @Test
    public void resetTest() {
        assertEquals(0, array.mutations());
        assertEquals(0, array.accesses());
        for (int i = 0; i < 15; i++) {
            array.put(i, Integer.toString(i));
            array.get(i);
        }
        assertEquals(15, array.mutations());
        assertEquals(15, array.accesses());
        array.reset();
        assertEquals(0, array.mutations());
        assertEquals(0, array.accesses());
    }
}
