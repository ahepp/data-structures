/* TODO - write the main method.
 * Unique.java
 */

package hw1;

/** A class with a main method for printing out unique numbers. 
 */
public final class Unique {

    // Make checkstyle happy.
    private Unique() {
        throw new AssertionError("Can not instantiate class Unique\n");
    }

    /**
     * A main method to print the unique numerical command line arguments.
     * @param args The string array of arguments in the command line.
     */
    public static void main(String[] args) {
        boolean found = false;
        int size = 0;
        String[] uniques = new String[size];
        for (String i: args) {
            if (!i.matches("-?\\d+")) {
                throw new IllegalArgumentException("Enter only integers!");
            }
            for (String j: uniques) {
                if (i.equals(j)) {
                    found = true;
                }
            }
            if (!found) {
                uniques = resize(uniques, size);
                uniques[size] = i;
                size++;
            }
            found = false;
        }
        for (String t: uniques) {
            System.out.println(t);
        }
    }

    private static String[] resize(String[] arr, int cap) {
        String[] temp = new String[cap + 1];
        System.arraycopy(arr, 0, temp, 0, cap);
        return temp;
    }
}
