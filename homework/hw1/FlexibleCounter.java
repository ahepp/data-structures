/* TODO - write the methods below.
 * FlexibleCounter.java
 */

package hw1;

/** Class for a counter with flexible starting and incrementing values. */
public class FlexibleCounter implements ResetableCounter {

    private int count;
    private final int increment;
    private final int initial;

    /**
     * Construct a new FlexibleCounter.
     * @param initialValue The value to start at.
     * @param incrementValue The value to increment the counter by.
     * @throws IllegalArgumentException If incrementValue is negative.
     */
    public FlexibleCounter(int initialValue, int incrementValue) {
        if (incrementValue < 0) {
            throw new IllegalArgumentException(
                    "Increment value must be non-negative.");
        }
        count = initialValue;
        increment = incrementValue;
        initial = initialValue;
    }

    @Override
    public void reset() {
        count = initial;
    }

    @Override
    public int value() {
        return count;
    }

    @Override
    public void up() {
        count += increment;
    }

    @Override
    public void down() {
        count -= increment;
    }
}
