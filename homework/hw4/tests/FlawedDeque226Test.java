package hw4.tests;


import hw4.Deque226;
import hw4.FlawedDeque226;
import exceptions.EmptyException;
import exceptions.ExampleException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class FlawedDeque226Test {

    // The unit being tested
    private Deque226<String> stringDequeue;

    @Before
    public void setupDequeue() {
        this.stringDequeue = new FlawedDeque226<String>();
    }

    @Test(expected = ExampleException.class)
    public void exampleTest() {
        throw new ExampleException();
    }

    @Test
    public void emptyDeque() {
        assertEquals(true, stringDequeue.empty());
    }

    @Test(expected = EmptyException.class)
    public void emptyFront() {
        stringDequeue.front();
    }

    @Test(expected = EmptyException.class)
    public void emptyBack() {
        stringDequeue.back();
    }

    @Test(expected = EmptyException.class)
    public void removeEmptyFront() {
        stringDequeue.removeFront();
    }

    @Test(expected = EmptyException.class)
    public void removeEmptyBack() {
        stringDequeue.removeBack();
    }

    @Test
    public void lengthTest() {
        assertEquals(true, stringDequeue.empty());
        assertEquals(0, stringDequeue.length());
    }

    @Test
    public void testSameInsertFront() {
        for (int i = 0; i < 150; i++) {
            stringDequeue.insertFront(Integer.toString(i));
        }
        assertEquals(false, stringDequeue.empty());
        assertEquals(150, stringDequeue.length());
    }

    @Test
    public void testInsertFront() {
        stringDequeue.insertFront("test");
        assertEquals("test", stringDequeue.front());
        for (int i = 0; i < 150; i++) {
            stringDequeue.insertFront("test2");
        }
        assertEquals(false, stringDequeue.empty());
        assertEquals(151, stringDequeue.length());
    }

    @Test
    public void testInsertBack() {
        stringDequeue.insertBack("test");
        assertEquals(1, stringDequeue.length());
        assertEquals("test", stringDequeue.back());
        for (int i = 0; i < 150; i++) {
            stringDequeue.insertBack("test2");
        }
        assertEquals(false, stringDequeue.empty());
        assertEquals(151, stringDequeue.length());
    }

    @Test
    public void testRemoveFront() {
        stringDequeue.insertFront("test");
        assertEquals("test", stringDequeue.front());
        stringDequeue.removeFront();
        assertEquals(true, stringDequeue.empty());
        for (int i = 0; i < 150; i++) {
            stringDequeue.insertFront("test2");
        }
        int len = stringDequeue.length();
        assertEquals(150, stringDequeue.length());
        stringDequeue.removeFront();
        assertEquals(false, stringDequeue.empty());
        assertEquals(--len, stringDequeue.length());
        for (int i = 0; i < 100; i++) {
            stringDequeue.removeFront();
        }
        assertEquals(false, stringDequeue.empty());
        assertEquals(len -= 100, stringDequeue.length());
        for (int i = 0; i < len; i++) {
            stringDequeue.removeFront();
        }
        assertEquals(true, stringDequeue.empty());
        assertEquals(0, stringDequeue.length());
    }

    @Test
    public void testRemoveBack() {
        stringDequeue.insertBack("test");
        assertEquals("test", stringDequeue.front());
        stringDequeue.removeBack();
        assertEquals(true, stringDequeue.empty());
        for (int i = 0; i < 150; i++) {
            // Using insertFront() here because we know it works,
            // insertBack() does not.
            stringDequeue.insertFront("test2");
        }
        assertEquals(150, stringDequeue.length());
        int len = stringDequeue.length();
        stringDequeue.removeBack();
        assertEquals(false, stringDequeue.empty());
        assertEquals(--len, stringDequeue.length());
        for (int i = 0; i < 100; i++) {
            stringDequeue.removeBack();
        }
        assertEquals(false, stringDequeue.empty());
        assertEquals(len -= 100, stringDequeue.length());
        for (int i = 0; i < len; i++) {
            stringDequeue.removeBack();
        }
        assertEquals(true, stringDequeue.empty());
        assertEquals(0, stringDequeue.length());
    }

    @Test
    public void testFront() {
        stringDequeue.insertFront("test");
        assertEquals("test", stringDequeue.front());
        stringDequeue.removeFront();
        assertEquals(true, stringDequeue.empty());
        for (int i = 0; i < 100; i++) {
            stringDequeue.insertFront(Integer.toString(i));
        }
        for (int j = stringDequeue.length() - 1; j >= 0; j--) {
            assertEquals(Integer.toString(j), stringDequeue.front());
            stringDequeue.removeFront();
        }
        assertEquals(true, stringDequeue.empty());
    }

    @Test
    public void testBack() {
        stringDequeue.insertBack("test");
        assertEquals("test", stringDequeue.back());
        stringDequeue.removeBack();
        assertEquals(true, stringDequeue.empty());
        //We know this doesn't quite work properly, so have to test a little
        // differently than testFront()
        for (int i = 1; i < 250; i++) {
            stringDequeue.insertBack(Integer.toString(i));
            // Only check if true when length is not a power of 2 to avoid
            // problems in insertBack()
            if ((stringDequeue.length() & (stringDequeue.length() - 1)) != 0) {
                assertEquals(Integer.toString(i), stringDequeue.back());
            }

        }
    }

    @Test
    public void otherTestBack() {
        stringDequeue.insertFront("test");
        assertEquals("test", stringDequeue.back());
        stringDequeue.removeBack();
        assertEquals(true, stringDequeue.empty());
        for (int i = 1; i < 250; i++) {
            // Again using insertFront() here because it works properly.
            stringDequeue.insertFront(Integer.toString(i));

        }
        for (int i = 0; i > 250; i++) {
            assertEquals(Integer.toString(i), stringDequeue.back());
            stringDequeue.removeBack();
        }
    }
}
