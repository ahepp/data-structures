// Calc.java

package hw4;

import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;

/** A program for an RPN calculator that uses a stack. */
public final class Calc {

    // Hush checkstyle
    private Calc() {}

    // Helper function to reduce cyclomatic complexity, handles all the binary
    // operations.
    private static void handleOp(String current, Stack<Integer> calc) {
        int first = 0;
        int second = 0;
        try {
            // See if we can pop the top two values on the stack
            first = calc.pop();
            second = calc.pop();
        }
        catch (EmptyStackException e) {
            // Catch the exception if we can't
            System.err.println("ERROR: Not enough arguments.");
        }
        // Does all the binary operations, second being the first argument in
        // them all.
        switch (current) {
            case "+":
                calc.push(second + first);
                break;
            case "-":
                calc.push(second - first);
                break;
            case "*":
                calc.push(second * first);
                break;
            case "/":
                // Check to make sure we aren't tyring to divide by 0
                if (first == 0) {
                    System.err.println("ERROR: Divide by 0.");
                    // If we were, push the popped items back to the stack in
                    // proper order
                    calc.push(second);
                    calc.push(first);
                    break;
                }
                calc.push(second / first);
                break;
            case "%":
                // Check to see if we were trying to mod by 0
                if (first == 0) {
                    System.err.println("ERROR: Mod by 0.");
                    // If we were, push the popped items back to the stack in
                    // proper order
                    calc.push(second);
                    calc.push(first);
                    break;
                }
                calc.push(second % first);
                break;
            default:
                //nothing to do here, just making checkstyle happy
                break;
        }
    }

    // Helper function to reduce cyclomatic complexity, handles all the special
    // operations.
    private static void handleSpec(String current, Stack<Integer> calc) {
        switch (current) {
            case "!":
                // Exit on "!"
                System.exit(0);
                break;
            case "?":
                // Print the entire stack
                System.out.println(calc.toString());
                break;
            case ".":
                try {
                    // See if we can pop the value from the stack
                    System.out.println(calc.pop());
                }
                catch (EmptyStackException e) {
                    // Catch exception if we can't pop (meaning stack is empty)
                    System.err.println("ERROR: Not enough arguments.");
                }
                break;
            default:
                //nothing to do here, just making checkstyle happy
                break;
        }
    }

    /**
     * The main function.
     * @param args Not used.
     */
    public static void main(String[] args) {
        Stack<Integer> calc = new Stack<>();
        Scanner sc = new Scanner(System.in);
        String current;
        // Keep reading while there is still input to be read
        while (sc.hasNext()) {
            // Take in input into a string
            current = sc.next();
            // See if input is an integer
            if (current.matches("-?\\d+")) {
                try {
                    int num = Integer.parseInt(current);
                    calc.push(num);
                }
                catch (NumberFormatException e) {
                    System.err.println("ERROR: bad token");
                }
                // Push to stack
            }
            // See if input is a valid binary operation
            else if (current.matches("[-+/*%]")) {
                handleOp(current, calc);
            }
            // See if input is a valid "special" character, as specified in
            // the assignment
            else if (current.matches("[!.?]")) {
                handleSpec(current, calc);
            }
            //Otherwise input is a bad token
            else {
                System.err.println("ERROR: bad token");
            }
        }
    }
}
