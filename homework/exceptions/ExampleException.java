package exceptions;

/**
 Exception that is just an example included in this
 assignment to help illustrate some helpful syntax!
 */
public class ExampleException extends RuntimeException {
    private static final long serialVersionUID = 0L;
}
