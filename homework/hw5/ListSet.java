package hw5;

import exceptions.EmptyException;
import exceptions.PositionException;

import java.util.Iterator;


/**
 * Set implemented using our abstract List, with a LinkedList.
 *
 * @param <T> Element type.
 */
public class ListSet<T> implements Set<T> {

    protected List<T> list;

    public ListSet() {
        list = new LinkedList<T>();
    }

    @Override
    public void insert(T t) {
        if (this.has(t)) {
            return;
        }
        this.list.insertBack(t);
    }

    @Override
    public void remove(T t) {
        Position<T> pos = this.find(t);
        if (pos == null) {
            return;
        }
        this.list.remove(pos);
    }

    @Override
    public boolean has(T t) {
        return this.find(t) != null;
    }

    @Override
    public int size() {
        return this.list.length();
    }

    @Override
    public Iterator<T> iterator() {
        return this.list.iterator();
    }

    protected Position<T> find(T t) {
        try {
            Position<T> pos = this.list.front();
            while (! pos.get().equals(t)) {
                pos = this.list.next(pos);
            }
            return pos;
        } catch (EmptyException ee) {
            return null;
        } catch (PositionException pe) {
            return null;
        }
    }
}
