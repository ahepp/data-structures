package hw5;

/**
 * Set implemented using plain Java arrays.
 *
 * accessed. Override the relevant method(s) from ArraySet.
 * 
 * @param <T> Element type.
 */
public class TransposeArraySet<T> extends ArraySet<T> {

    /**
     * Method to find an element in a list and swapping the found element
     * with the element in front of it.
     * @param t element to find
     * @return the position object of the element, or -1 if not found
     */
    @Override
    protected int find(T t) {
        int pos = super.find(t);
        // Make sure we can swap the elements, e.g. the element was found and
        // we're not already at index 0.
        if (pos == -1 || pos == 0) {
            return pos;
        }
        // Swap them here
        T temp = this.data[pos];
        this.data[pos] = this.data[pos - 1];
        this.data[pos - 1] = temp;
        return pos;
    }

    /**
     * Method to remove a particular element from a list.
     * @param t element to find and remove.
     */
    @Override
    public void remove(T t) {
        // Have to use parent method here or else element will not get
        // deleted properly
        int p = super.find(t);
        if (p == -1) {
            return;
        }
        for (int i = p; i < this.used - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        this.used -= 1;
    }
}
