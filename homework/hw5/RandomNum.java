package hw5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public final class RandomNum {

    private RandomNum() {}

    public static void main(String[] args) throws IOException {
        Random num = new Random();
        String writer;
        BufferedWriter buff = new BufferedWriter(new FileWriter(
                "homework/hw5/randos.txt"));

        for (int i = 0; i < 10000000; i++) {
            writer = Integer.toString((num.nextInt()));
            buff.write(writer + "\n");
        }
        buff.close();
    }
}
