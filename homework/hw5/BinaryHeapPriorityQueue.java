package hw5;

import exceptions.EmptyException;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Priority queue implemented as a binary heap.
 *
 * Use the ranked array representation of a binary heap!
 * Keep the arithmetic simple by sticking a null into slot 0 of the
 * ArrayList; wasting one reference is an okay price to pay for nicer
 * code.
 *
 * @param <T> Element type.
 */
public class BinaryHeapPriorityQueue<T extends Comparable<? super T>>
    implements hw5.PriorityQueue<T> {

    // The default comparator uses the "natural" ordering.
    private static class DefaultComparator<T extends Comparable<? super T>>
        implements Comparator<T> {
        public int compare(T t1, T t2) {
            return t1.compareTo(t2);
        }
    }

    private Comparator<T> cmp;
    private ArrayList<T> queue;
    private int length;

    /**
     * A binary heap using the "natural" ordering of T.
     */
    public BinaryHeapPriorityQueue() {
        this(new DefaultComparator<>());
    }

    /**
     * A binary heap using the given comparator for T.
     * @param cmp Comparator to use.
     */
    public BinaryHeapPriorityQueue(Comparator<T> cmp) {
        this.cmp = cmp;
        this.queue = new ArrayList<>();
        // Add a null to position 0 to make the math way easier.
        this.queue.add(null);
        this.length = 0;
    }

    /**
     * Helper method to just swap two positions in an ArrayList.
     * @param a first element to swap
     * @param b second element to swap
     */
    private void swap(int a, int b) {
        T temp = this.queue.get(a);
        this.queue.set(a, this.queue.get(b));
        this.queue.set(b, temp);
    }

    /**
     * Helper method to move an inserted element up to its location,
     * specified by the comparator
     */
    private void siftUp() {
        int pos = this.length;
        // On every level of the tree, check is the current position is
        // larger than its children.
        while (pos > 1 && this.cmp.compare(this.queue.get(pos),
                this.queue.get(pos / 2)) > 0) {
            // If so, swap them.
            swap(pos, pos / 2);
            pos /= 2;
        }
    }

    /**
     * Helper method to trickle a value down into its designated place using
     * the specified comparator.
      */
    private void siftDown() {
        int pos = 1;
        int child;
        // Go until the level above the lowest leafs.
        while (pos * 2 < this.length) {
            child = pos * 2 + 1;
            // Checks which child is bigger (or if there's only one child).
            if (child >= this.length ||
                    this.cmp.compare(this.queue.get(pos * 2 + 1),
                    this.queue.get(pos * 2)) <= 0) {
                child = pos * 2;
            }
            // Checks if child is bigger than parent.
            if (this.cmp.compare(this.queue.get(pos),
                    this.queue.get(child)) < 0) {
                // If so, swap them.
                swap(pos, child);
            }
            // Early break to save computational time if in right place.
            else {
                return;
            }
            pos = child;
        }
    }

    /**
     * Method to insert a new element and put it in its proper place.
     * @param t Value to insert.
     */
    @Override
    public void insert(T t) {
        this.queue.add(t);
        this.length++;
        this.siftUp();
    }

    /**
     * Method to remove the root of the tree and re-"sort" it.
     * @throws EmptyException if heap is empty.
     */
    @Override
    public void remove() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        // Swaps the last index with the first, then removes the last so
        // there's no need to shift anything around.
        this.swap(1, this.length);
        this.queue.remove(this.length);
        this.length--;
        this.siftDown();
    }

    /**
     * Method to return the root of the tree.
     * @return the root of the tree.
     * @throws EmptyException if heap is empty.
     */
    @Override
    public T best() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        return queue.get(1);
    }

    /**
     * Method to check if a heap is empty.
     * @return true if heap is empty, false otherwise.
     */
    @Override
    public boolean empty() {
        return queue.size() == 1;
    }

    /**
     * Simple toString() method, just outputs the ArrayList that the heap is
     * composed of.
     * @return a string of the heap ArrayList.
     */
    @Override
    public String toString() {
        return this.queue.toString();
    }

}
