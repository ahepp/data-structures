package hw5.tests;

import hw5.Set;
import hw5.TransposeArraySet;

/** Instantiate the ArraySet to test. */
public class MoveToFrontListTest extends SetTest {
    @Override
    protected Set<String> createUnit() {
        return new TransposeArraySet<>();
    }
}
