package hw5.tests;

import exceptions.EmptyException;
import hw5.PriorityQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testing implementations of the PriorityQueue interface.
 */
public abstract class PriorityQueueTest {
    private PriorityQueue<String> unit;

    private PriorityQueue<Integer> unit2;

    protected abstract PriorityQueue<String> createUnit();

    protected abstract PriorityQueue<Integer> createUnit2();

    @Before
    public void setupTests() {
        unit = this.createUnit();
        unit2 = this.createUnit2();
    }

    @Test
    public void siftTest() {
        int insert = 10;
        unit2.insert(insert);
        assertFalse(unit2.empty());
        assertEquals((long) insert, (long) unit2.best());
        unit2.remove();
        assertTrue(unit2.empty());
        for (int i = 1; i <= 150; i++) {
            unit2.insert(i);
        }
        for (int i = 150; i > 1; i--) {
            assertEquals((long) i, (long) unit2.best());
            unit2.remove();
        }
    }


    @Test
    public void newQueueEmpty() {
        assertTrue(unit.empty());
    }

    @Test(expected=EmptyException.class)
    public void newQueueNoBest() {
        String s = unit.best();
    }

    @Test(expected=EmptyException.class)
    public void newQueueNoRemove() {
        unit.remove();
    }

    @Test
    public void insertNotEmpty() {
        unit.insert("Paul");
        assertFalse(unit.empty());
    }

    @Test
    public void insertBest() {
        unit.insert("Paul");
        assertEquals("Paul", unit.best());

        unit.insert("Peter");
        assertEquals("Peter", unit.best());

        unit.insert("Mary");
        assertEquals("Peter", unit.best());
    }

    @Test
    public void insertRemoveInOrder() {
        String d[] = {"Zion", "Tom", "Karen", "Ed", "Billy", "Beverly"};
        for (int i = d.length-1; i >= 0; i--) {
            unit.insert(d[i]);
        }

        for (String s: d) {
            assertEquals(s, unit.best());
            unit.remove();
        }
    }
}
