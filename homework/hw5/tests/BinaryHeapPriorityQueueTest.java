package hw5.tests;

import hw5.BinaryHeapPriorityQueue;
import hw5.PriorityQueue;

/** Instantiate the BinaryHeapPriorityQueue to test. */
public class BinaryHeapPriorityQueueTest extends PriorityQueueTest {
    @Override
    protected PriorityQueue<String> createUnit() {
        return new BinaryHeapPriorityQueue<>();
    }

    protected PriorityQueue<Integer> createUnit2() {
        return new BinaryHeapPriorityQueue<>();
    }

}
