package hw5.tests;

import hw5.Set;
import hw5.ArraySet;

/** Instantiate the ArraySet to test. */
public class ArraySetTest extends SetTest {
    @Override
    protected Set<String> createUnit() {
        return new ArraySet<>();
    }
}
