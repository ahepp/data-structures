package hw5;

/**
 * Set implemented using our abstract List, with a LinkedList.
 *
 * accessed. Override the relevant method(s) from ListSet.
 *
 * @param <T> Element type.
 */

public class MoveToFrontListSet<T> extends ListSet<T> {

    /**
     * Method to find an element in a list and move that element to the front
     * of the list for decreases time finding it next time.
     * @param t element to find
     * @return the position object of the element, or null if not found
     */
    @Override
    protected Position<T> find(T t) {
        Position<T> pos = super.find(t);
        // Check if it was found, or if it is in the front of the list, or if
        // the list is empty (the three cases where we can't move to front).
        if (pos == null || pos == this.list.front()
                || this.list.length() == 0) {
            return pos;
        }
        // Make a new node at the front with the value of the found node.
        this.list.insertFront(t);
        // Delete the position of the old node so we don't have duplicates.
        this.list.remove(pos);
        return pos;
    }

    /**
     * Method to remove a particular element from a list.
     * @param t element to find and remove.
     */
    @Override
    public void remove(T t) {
        // Have to use parent method here or else element will not get
        // deleted properly.
        Position<T> pos = super.find(t);
        if (pos == null) {
            return;
        }
        this.list.remove(pos);
    }
}
