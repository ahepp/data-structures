package hw6;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Count unique words.
 *
 * Read text from standard input, count how often each unique word appears,
 * write the results to standard output. Note that the definition of "word"
 * is rather arbitrary and won't make the linguists among you very happy.
 */
public final class Words {
    private static Map<String, Integer> data;

    // Make checkstyle happy.
    private Words() {}

    /**
     * Main method.
     * @param args Command line arguments (ignored).
     * @throws IOException if there is an IO exception
     */
    public static void main(String[] args) throws IOException {
        data = new TreapMap<>();
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            // The regular expression splits strings on whitespace and
            // non-word characters (anything except [a-zA-Z_0-9]). Far
            // from perfect, but close enough for this simple program.
            String[] words = s.split("[^a-zA-Z0-9]");
            for (String word: words) {
                if (word.length() <= 1) {
                    // Skip "short" words, most of which just "dirty up"
                    // the statistics.
                    continue;
                }
                if (data.has(word.toLowerCase())) {
                    data.put(word.toLowerCase(),
                            data.get(word.toLowerCase()) + 1);
                } else {
                    data.insert(word.toLowerCase(), 1);
                }
            }
        }

        File file = new File(
                "/Users/alexanderhepp/Documents/wordsOutput.txt");
        BufferedWriter buff = new BufferedWriter(new FileWriter(file));
        for (String word: data) {
            buff.write(word + ": " + data.get(word) + "\n");
        }

        buff.close();
    }
}
