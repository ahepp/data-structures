package hw6.tests;

import hw6.AvlTreeMap;
import hw6.OrderedMap;
import java.util.ArrayList;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AvlTreeMapTest extends OrderedMapTest {

    @Override
    protected OrderedMap<Integer, String> createUnit() {
        return new AvlTreeMap<>();
    }

    /*
    Should look like
    1
     \
      2
       \
        3
    At first, then go to
        2
       / \
      1   3
    After Rotation
     */
    @Test
    public void simpleRightBalanceTest() {
        unit.insert(1, "one");
        unit.insert(2, "two");
        unit.insert(3, "three");
        assertEquals("2: two, 1: one, 3: three",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit) {
            al.add(i);
        }
        assertEquals(3, al.size());
        assertEquals(1, (int) al.get(0));
        assertEquals(2, (int) al.get(1));
        assertEquals(3, (int) al.get(2));

    }

    /*
    Should look like
        3
       /
      2
     /
    1
    At first, then go to
        2
       / \
      1   3
    After Rotation
     */
    @Test
    public void simpleLeftBalanceTest() {
        unit.insert(3, "three");
        unit.insert(2, "two");
        unit.insert(1, "one");
        assertEquals("2: two, 1: one, 3: three",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit) {
            al.add(i);
        }
        assertEquals(1, (int) al.get(0));
        assertEquals(2, (int) al.get(1));
        assertEquals(3, (int) al.get(2));
    }

    /*
    Should look like
        1
         \
          3
         /
        2
    At first, then go to
        2
       / \
      1   3
    After Rotation
     */
    @Test
    public void simpleRightLeftBalanceTest() {
        unit.insert(1, "one");
        unit.insert(3, "three");
        unit.insert(2, "two");
        assertEquals("2: two, 1: one, 3: three",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit) {
            al.add(i);
        }
        assertEquals(1, (int) al.get(0));
        assertEquals(2, (int) al.get(1));
        assertEquals(3, (int) al.get(2));
    }

    /*
    Should look like
        3
       /
      1
       \
        2
    At first, then go to
        2
       / \
      1   3
    After Rotation
     */
    @Test
    public void simpleLeftRightBalanceTest() {
        unit.insert(1, "one");
        unit.insert(3, "three");
        unit.insert(2, "two");
        assertEquals("2: two, 1: one, 3: three",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit) {
            al.add(i);
        }
        assertEquals(1, (int) al.get(0));
        assertEquals(2, (int) al.get(1));
        assertEquals(3, (int) al.get(2));
    }

    /*
    Should look like
        1
         \
          2
           \
            3
             \
              4
               \
                5
                 6
                  \
                   7
    At first, then go to
        4
       / \
      2   5
     / \   \
    1   3   6
    After Rotation
     */
    @Test
    public void multipleLeftTest() {
        unit.insert(1, "one");
        unit.insert(2, "two");
        unit.insert(3, "three");
        unit.insert(4, "four");
        unit.insert(5, "five");
        unit.insert(6, "six");
        assertEquals("4: four, 2: two, 5: five, 1: one, 3: three, 6: six",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit) {
            al.add(i);
        }
        assertEquals(1, (int) al.get(0));
        assertEquals(2, (int) al.get(1));
        assertEquals(3, (int) al.get(2));
        assertEquals(4, (int) al.get(3));
        assertEquals(5, (int) al.get(4));
        assertEquals(6, (int) al.get(5));

    }

    /*
   Should look like
             6
            /
           5
          /
         4
        /
       3
      /
     2
    /
   1
   At first, then go to
       3
      / \
     2   5
    /   / \
   1   4   6
   After Rotation
    */
    @Test
    public void multipleRightTest() {
        unit.insert(6, "six");
        unit.insert(5, "five");
        unit.insert(4, "four");
        unit.insert(3, "three");
        unit.insert(2, "two");
        unit.insert(1, "one");
        assertEquals("3: three, 2: two, 5: five, 1: one, 4: four, 6: six",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
    }

    /*
    Should look like
        12
       /  \
      10   14
             \
              15
    At first, then go to
        14
       /  \
      12  15
    After Rotation
    */
    @Test
    public void removeLeftRotationTest() {
        unit.insert(12, "12");
        unit.insert(10, "10");
        unit.insert(14, "14");
        unit.insert(15, "15");
        assertEquals("12: 12, 10: 10, 14: 14, 15: 15",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        unit.remove(10);
        assertEquals("14: 14, 12: 12, 15: 15",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
    }

    /*
    Should look like
        12
       /  \
      10   14
     /
    8
    At first, then go to
        10
       /  \
      8   12
    After Rotation
    Then
        12
       /  \
      10   14
     /
    8
    and
        10
       /  \
      8   14
    */
    @Test
    public void removeRightRotationTest() {
        unit.insert(12, "12");
        unit.insert(10, "10");
        unit.insert(14, "14");
        unit.insert(8, "8");
        assertEquals("12: 12, 10: 10, 14: 14, 8: 8",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        unit.remove(14);
        assertEquals("10: 10, 8: 8, 12: 12",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        unit.insert(14, "14");
        unit.remove(12);
        assertEquals("10: 10, 8: 8, 14: 14",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
    }

    @Test
    public void moreComplexRemoveTest() {
        unit.insert(10, "");
        unit.insert(5, "");
        unit.insert(20, "");
        unit.insert(2, "");
        unit.insert(7, "");
        unit.insert(15, "");
        unit.insert(25, "");
        unit.insert(1, "");
        unit.insert(4, "");
        unit.insert(6, "");
        unit.insert(9, "");
        unit.insert(17, "");
        unit.insert(3, "");
        unit.insert(8, "");
        assertEquals("10: , 5: , 20: , 2: , 7: , 15: , 25: , 1: , 4: , " +
                "6: , 9: , 17: , 3: , 8: ",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        unit.remove(10);
        assertEquals("9: , 5: , 20: , 2: , 7: , 15: , 25: , 1: , " +
                "4: , 6: , 8: , 17: , 3: ",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        assertEquals(13, unit.size());

    }

    /*
        12
        / \
      5    14
     / \   / \
    3   8 13 18
       / \
      7  10
     */
    @Test
    public void removeTest() {
        unit.insert(12, "");
        unit.insert(14, "");
        unit.insert(5, "");
        unit.insert(8, "");
        unit.insert(3, "");
        unit.insert(13, "");
        unit.insert(18, "");
        unit.insert(7, "");
        unit.insert(10, "");
        assertEquals("12: , 5: , 14: , 3: , 8: , 13: , 18: , 7: , 10: ",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());
        unit.remove(5);
        assertEquals("12: , 8: , 14: , 3: , 10: , 13: , 18: , 7: ",
                ((AvlTreeMap<Integer, String>) unit).breadthFirst());


    }

    // Worst case scenario, check if find, insert run on log time. Compared
    // to same function in BinarySearchTreeMapTest (which can only handle
    // like 10000 values) and the avl tree is much quicker, and doesn't throw
    // stackOverflows.
    @Test
    public void benchmark() {
        for (int i = 0; i < 1000000; i++) {
            unit.insert(i, Integer.toString(i));
        }
        unit.get(65532);
        unit.remove(92819);
    }


}
