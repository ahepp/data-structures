package hw6.tests;

import hw6.OrderedMap;
import hw6.TreapMap;
import java.util.ArrayList;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TreapMapTest extends OrderedMapTest {

    @Override
    protected OrderedMap<Integer, String> createUnit() {
        return new TreapMap<>();
    }

    /*
    Should go in as:
        7/3
        /
      2/0
    and rotate to.
        2/0
          \
           7/3
     */
    @Test
    public void simpleRightTest() {
        TreapMap<Integer, String> unit2 = ((TreapMap<Integer, String>) unit);
        unit2.insert(7, "3", 3);
        unit2.insert(2, "0", 0);
        assertEquals("2: 0, 7: 3", unit2.breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit) {
            al.add(i);
        }
        assertEquals(2, (int) al.get(0));
        assertEquals(7, (int) al.get(1));


    }

    /*
    Should go in as:
        7/3
         \
         9/0
    and be rotated to
       9/0
        /
      7/3
     */
    @Test
    public void simpleLeftTest() {
        ((TreapMap<Integer, String>) unit).insert(7, "3", 7);
        ((TreapMap<Integer, String>) unit).insert(9, "0", 0);
        assertEquals("9: 0, 7: 3",
                ((TreapMap<Integer, String>) unit).breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit) {
            al.add(i);
        }
        assertEquals(7, (int) al.get(0));
        assertEquals(9, (int) al.get(1));

    }

    /*
    Should go in as:
            2/0
            /  \
          1/7  5/1
               /  \
             4/7  8/2
             /    /
           3/9  7/3
     */
    @Test
    public void moreComplexTest() {
        TreapMap<Integer, String> unit2 = ((TreapMap<Integer, String>) unit);
        unit2.insert(7, "3", 3);
        unit2.insert(2, "0", 0);
        unit2.insert(1, "7", 7);
        unit2.insert(8, "2", 2);
        unit2.insert(3, "9", 9);
        unit2.insert(5, "1", 1);
        unit2.insert(4, "7", 7);
        assertEquals("2: 0, 1: 7, 5: 1, 4: 7, 8: 2, 3: 9, 7: 3",
                unit2.breadthFirst());
        ArrayList<Integer> al = new ArrayList<>();
        for (int i: unit2) {
            al.add(i);
        }
        assertEquals(1, (int) al.get(0));
        assertEquals(2, (int) al.get(1));
        assertEquals(3, (int) al.get(2));
        assertEquals(4, (int) al.get(3));
        assertEquals(5, (int) al.get(4));
        assertEquals(7, (int) al.get(5));
        assertEquals(8, (int) al.get(6));

    }

    @Test
    public void simpleRemoveTest() {
        TreapMap<Integer, String> unit2 = ((TreapMap<Integer, String>) unit);
        unit2.insert(7, "3", 3);
        unit2.insert(2, "0", 0);
        unit2.insert(8, "5", 5);
        assertEquals("2: 0, 7: 3, 8: 5", unit2.breadthFirst());
        unit2.remove(8);
        assertEquals("2: 0, 7: 3", unit2.breadthFirst());
        unit2.insert(8, "5", 5);
        unit2.remove(2);
        assertEquals("7: 3, 8: 5", unit2.breadthFirst());
        unit2.insert(2, "0", 0);
        unit2.remove(7);
        assertEquals("2: 0, 8: 5", unit2.breadthFirst());
    }

    /*
    Should go in as:
            20/0
            /  \
          16/2  22/1
         /  \
      14/5   18/6
       / \    /
    12/7 15/7 17/8
     \
      10/9
     */
    @Test
    public void removeRotateTest() {
        TreapMap<Integer, String> unit2 = ((TreapMap<Integer, String>) unit);
        unit2.insert(20, "0", 0);
        unit2.insert(22, "1", 1);
        unit2.insert(16, "2", 2);
        unit2.insert(18, "6", 6);
        unit2.insert(17, "8", 8);
        unit2.insert(14, "5", 5);
        unit2.insert(15, "7", 7);
        unit2.insert(12, "7", 7);
        unit2.insert(10, "9", 9);
        assertEquals("20: 0, 16: 2, 22: 1, 14: 5, 18: 6, 12: 7, 15: 7, 17: 8," +
                        " 10: 9", unit2.breadthFirst());
        unit2.remove(16);
        assertEquals("20: 0, 14: 5, 22: 1, 12: 7, 18: 6, 10: 9, 15: 7, 17: 8",
                unit2.breadthFirst());

    }

    /*
     Should go in as:
            10/0
            /  \
          4/1  20/2
         /  \    /  \
      1/5   6/8 16/6 25/3
                     /  \
                  24/7 26/6
      Then be
            10/0
            /  \
          4/1  25/3
         /  \    /  \
      1/5   6/8 16/6 26/6
                  \
                  24/7
     */
    @Test
    public void removeRotate2Test() {
        TreapMap<Integer, String> unit2 = ((TreapMap<Integer, String>) unit);
        unit2.insert(10, "0", 0);
        unit2.insert(4, "1", 1);
        unit2.insert(1, "5", 5);
        unit2.insert(6, "8", 8);
        unit2.insert(20, "2", 2);
        unit2.insert(16, "6", 6);
        unit2.insert(25, "3", 3);
        unit2.insert(24, "7", 7);
        unit2.insert(26, "4", 4);
        assertEquals("10: 0, 4: 1, 20: 2, 1: 5, 6: 8, 16: 6, 25: 3, 24: 7," +
                        " 26: 4", unit2.breadthFirst());
        unit2.remove(20);
        assertEquals("10: 0, 4: 1, 25: 3, 1: 5, 6: 8, 16: 6, 26: 4," +
                        " 24: 7", unit2.breadthFirst());
    }

    @Test
    public void randomTest() {
        // If insert cant parse second value as an integer when using the
        // testing constructor, a random priority will be assigned.
        unit.insert(5, "a");
        unit.insert(10, "c");
        assertTrue(((TreapMap<Integer, String>) unit).getPriority(5)
                != (int) 'a');
    }

    // Worst case scenario, check if find, insert run on log time. Compared
    // to same function in BinarySearchTreeMapTest (which can only handle
    // like 10000 values) and the avl tree is much quicker, and doesn't throw
    // stackOverflows.
    @Test
    public void benchmark() {
        for (int i = 0; i < 1000000; i++) {
            unit.insert(i, Integer.toString(i));
        }
        unit.get(65532);
        unit.remove(92819);
    }
}
