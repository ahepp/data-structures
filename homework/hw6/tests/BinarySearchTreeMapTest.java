package hw6.tests;

import hw6.BinarySearchTreeMap;
import hw6.OrderedMap;

import org.junit.Test;


public class BinarySearchTreeMapTest extends OrderedMapTest {
    @Override
    protected OrderedMap<Integer, String> createUnit() {
        return new BinarySearchTreeMap<>();
    }

    @Test
    public void benchmark() {
        for (int i = 0; i < 10000; i++) {
            unit.insert(i, Integer.toString(i));
        }
        unit.get(6532);
    }
}
