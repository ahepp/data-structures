package hw6.tests;

import hw6.OrderedMap;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public abstract class OrderedMapTest extends MapTest {

    @Override
    protected abstract OrderedMap<Integer, String> createUnit();


    @Test
    public void orderTest() {
        String[] val = {"a", "b", "c", "d", "e"};
        int[] key = {4, 3, 2, 1, 0};
        String[] right = {"e", "d", "c", "b", "a"};
        int count = 0;
        for (int i: key) {
            unit.insert(i, val[count]);
            count++;
        }
        count = 0;
        for (int k: unit) {
            assertEquals(right[count], unit.get(k));
            count++;
        }
    }
}
