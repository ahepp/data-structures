package hw6;

import java.util.Iterator;
import java.util.ArrayDeque;
import java.util.List;
import java.util.ArrayList;

/**
 * Implementation of an AVL. Mirrors Binary Search Tree pretty well, but does
 * rotations to keep balanced.
 * @param <K> Map key
 * @param <V> Map value
 */
public class AvlTreeMap<K extends Comparable<? super K>, V>
        implements OrderedMap<K, V> {

    // Inner node class, each holds a key (which is what we sort the
    // BST by) as well as a value. We don't need a parent pointer as
    // long as we use recursive insert/remove helpers.
    private class Node {
        Node left;
        Node right;
        K key;
        V value;
        int height;

        // Constructor to make node creation easier to read.
        Node(K k, V v) {
            // left and right default to null
            this.key = k;
            this.value = v;
            this.height = 1;
        }

        // Just for debugging purposes.
        public String toString() {
            return "Node<key: " + this.key
                    + "; value: " + this.value
                    + ">";
        }
    }

    private Node root;
    private int size;
    private StringBuilder stringBuilder;

    @Override
    public int size() {
        return this.size;
    }

    // Helper function to return the height of a given node.
    private int height(Node n) {
        if (n == null) {
            return 0;
        }
        return n.height;
    }

    // Copy-pasted method from BinarySearchTreeMap
    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("Enter a non-null key.");
        }
        Node n = this.root;
        while (n != null) {
            if (k.compareTo(n.key) < 0) {
                n = n.left;
            } else if (k.compareTo(n.key) > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.find(k) != null;
    }

    // Return node for given key, throw an exception
    // if the key is not in the tree.
    private Node findForSure(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }

    @Override
    public void put(K k, V v) {
        Node n = this.findForSure(k);
        n.value = v;
    }

    @Override
    public V get(K k) {
        Node n = this.findForSure(k);
        return n.value;
    }

    // Helper method to return the greater height of a nodes children.
    private int maxHeight(Node n) {
        if (n == null) {
            return 0;
        }
        if (this.height(n.left) > this.height(n.right)) {
            return this.height(n.left) + 1;
        }
        else {
            return this.height(n.right) + 1;
        }
    }

    // Function to perform a right rotation.
    private Node rotateRight(Node n) {
        Node a = n.left;
        Node b = a.right;

        a.right = n;
        n.left = b;

        n.height = this.maxHeight(n);
        a.height = this.maxHeight(a);

        return a;
    }

    // Function to perform a left rotation.
    private Node rotateLeft(Node n) {
        Node a = n.right;
        Node b = a.left;

        a.left = n;
        n.right = b;

        n.height = this.maxHeight(n);
        a.height = this.maxHeight(a);

        return a;
    }

    // Helper function to do double rotations....thanks cyclomatic complexity.
    private Node doDoubles(Node n, int bal, int childBal) {
        // Single Right case
        if (bal == 2 && childBal == 0) {
            return this.rotateRight(n);
        }

        // Single left case
        if (bal == -2 && childBal == 0) {
            return this.rotateLeft(n);
        }
        // Left Right Case
        if (bal == 2 && (childBal == -1)) {
            n.left = this.rotateLeft(n.left);
            return this.rotateRight(n);
        }

        // Right Left Case
        if (bal == -2 && childBal == 1) {
            n.right = this.rotateRight(n.right);
            return this.rotateLeft(n);
        }

        return n;
    }

    // Helper function to figure out which (if any) rotation operations to
    // perform.
    private Node doBalance(Node n) {

        int bal = this.balance(n);
        int childBal;
        if (bal > 0) {
            childBal = this.balance(n.left);
        }
        else {
            childBal = this.balance(n.right);
        }

        // Single Right case
        if (bal == 2 && childBal == 1) {
            return this.rotateRight(n);
        }

        // Single left case
        if (bal == -2 && childBal == -1) {
            return this.rotateLeft(n);
        }

        else {
            n = doDoubles(n, bal, childBal);
        }
        return n;
    }

    // Get the balance factor for a node
    private int balance(Node n) {
        if (n == null) {
            return 0;
        }
        return this.height(n.left) - this.height(n.right);
    }

    // Insert given key and value into subtree rooted
    // at given node; return changed subtree with new
    // node added. Computes height and rebalances if
    // needed.
    private Node insert(Node n, K k, V v) {
        if (n == null) {
            return new Node(k, v);
        }
        if (k.compareTo(n.key) < 0) {
            n.left = this.insert(n.left, k, v);
        } else if (k.compareTo(n.key) > 0) {
            n.right = this.insert(n.right, k, v);
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }

        n.height = this.maxHeight(n);

        n = this.doBalance(n);
        return n;
    }

    @Override
    public void insert(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.insert(this.root, k, v);
        this.size += 1;
    }

    // Return node with maximum key in subtree rooted
    // at given node. (Iterative version because once
    // again recursion has no advantage here.)
    private Node max(Node n) {
        while (n.right != null) {
            n = n.right;
        }
        return n;
    }

    // Remove node with given key from subtree rooted at
    // given node; return changed subtree with given key
    // missing. Computes new height for each node and re-
    // balances if needed.
    private Node remove(Node n, K k) {
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        if (k.compareTo(n.key) < 0) {
            n.left = this.remove(n.left, k);
        } else if (k.compareTo(n.key) > 0) {
            n.right = this.remove(n.right, k);
        } else {
            n = this.remove(n);
        }
        if (n == null) {
            return n;
        }

        n.height = maxHeight(n);
        n = this.doBalance(n);

        return n;
    }

    // Remove given node and return the remaining tree.
    // Easy if the node has 0 or 1 child; if it has two
    // children, find the predecessor, copy its data to
    // the given node (thus removing the key we need to
    // get rid off), then remove the predecessor node.
    private Node remove(Node n) {
        // 0 and 1 child
        if (n.right == null) {
            return n.left;
        }
        if (n.left == null) {
            return n.right;
        }

        // 2 children
        Node max = this.max(n.left);
        n.key = max.key;
        n.value = max.value;
        n.left = this.remove(n.left, max.key);

        return n;
    }

    @Override
    public V remove(K k) {
        // Need this additional find() for the V return value, because the
        // private remove() method cannot return that in addition to the new
        // root. If we had been smarter and used a void return type, we would
        // not need to do this extra work.
        V v = this.findForSure(k).value;
        this.root = this.remove(this.root, k);
        this.size -= 1;
        return v;
    }

    // Recursively add keys from subtree rooted at given node
    // into the given list.
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) {
            return;
        }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }

    // If we don't have a StringBuilder yet, make one;
    // otherwise just reset it back to a clean slate.
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }

    // Recursively append string representations of keys and
    // values from subtree rooted at given node.
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) {
            return;
        }
        this.toStringHelper(n.left, s);
        s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");
        this.toStringHelper(n.right, s);
    }

    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{");

        this.toStringHelper(this.root, this.stringBuilder);

        int length = this.stringBuilder.length();
        if (length > 1) {
            // If anything was appended at all, get rid of
            // the last ", " the toStringHelper put in.
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}");

        return this.stringBuilder.toString();
    }

    /**
     * Function to give Breadth-first traversal.
     * Decided to use a breadth-first toString() for testing purposes, I
     * found it easier to determine if rotations were working properly with
     * this over a depth-first approach.
     * @return string of traversal.
     */
    public String breadthFirst() {
        StringBuilder s = new StringBuilder();
        ArrayDeque<Node> q = new ArrayDeque<>();
        q.add(this.root);
        while (q.size() > 0) {
            Node n = q.removeFirst();
            s.append(n.key);
            s.append(": ");
            s.append(n.value);
            s.append(", ");
            if (n.left != null) {
                q.add(n.left);
            }
            if (n.right != null) {
                q.add(n.right);
            }
        }
        s.setLength(s.length() - 2);
        return s.toString();
    }
}
