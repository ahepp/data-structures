package hw6;

import java.util.Random;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.ArrayDeque;

/**
 * Implementation of a Treap, user must specify both key and value, instead of
 * the typical randomly assigned value priorities. Both K and V must be
 * comparable to work.
 * @param <K> Key to add by
 * @param <V> Value to rotate by
 */
public class TreapMap<K extends Comparable<? super K>, V>
        implements OrderedMap<K, V> {

    // Inner node class, each holds a key (which is what we sort the
    // BST by) as well as a value. We don't need a parent pointer as
    // long as we use recursive insert/remove helpers.
    private class Node {
        Node left;
        Node right;
        K key;
        V value;
        int priority;
        Random rand;

        // Constructor to make node creation easier to read.
        Node(K k, V v) {
            // left and right default to null
            this.key = k;
            this.value = v;
            this.rand = new Random();
            this.priority = Math.abs(this.rand.nextInt());
        }

        Node(K k, V v, int p) {
            this.key = k;
            this.value = v;
            this.priority = p;
        }

        // Just for debugging purposes.
        public String toString() {
            return "Node<key: " + this.key
                    + "; value: " + this.value
                    + "; priority: " + this.priority
                    + ">";
        }
    }

    private Node root;
    private int size;
    private StringBuilder stringBuilder;

    /**
     * Function to return the priority of a node.
     * @param k key to return priority for.
     * @return the priority of the node.
     */
    public int getPriority(K k) {
        Node n = this.findForSure(k);
        return n.priority;
    }

    @Override
    public int size() {
        return this.size;
    }

    // Copy-pasted method from BinarySearchTreeMap
    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("Enter a non-null key.");
        }
        Node n = this.root;
        while (n != null) {
            if (k.compareTo(n.key) < 0) {
                n = n.left;
            } else if (k.compareTo(n.key) > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.find(k) != null;
    }

    // Return node for given key, throw an exception
    // if the key is not in the tree.
    private Node findForSure(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }

    @Override
    public void put(K k, V v) {
        Node n = this.findForSure(k);
        n.value = v;
    }

    @Override
    public V get(K k) {
        Node n = this.findForSure(k);
        return n.value;
    }

    // Function to perform a right rotation.
    private Node rotateRight(Node n) {
        Node a = n.left;
        Node b = a.right;

        a.right = n;
        n.left = b;

        a.right = this.doBalance(n);
        return a;
    }

    // Function to perform a left rotation.
    private Node rotateLeft(Node n) {
        Node a = n.right;
        Node b = a.left;

        a.left = n;
        n.right = b;

        a.left = this.doBalance(n);
        return a;
    }

    // Function to test if both children exist, and if so see which is bigger
    // and perform the rotation.
    private Node balanceHelp(Node n) {
        if (n.left == null && n.right == null) {
            return n;
        }
        try {
            if (n.priority > n.left.priority && n.priority > n.right.priority) {
                if (n.left.priority > n.right.priority) {
                    return this.rotateLeft(n);
                }
                else {
                    return this.rotateRight(n);
                }
            }
        }
        catch (NullPointerException e) {
            // Grab that exception
        }
        return n;
    }

    // Helper function to figure out which (if any) rotation operations to
    // perform.
    private Node doBalance(Node n) {
        n = this.balanceHelp(n);
        try {
            if (n.priority > n.left.priority) {
                return this.rotateRight(n);
            }
        }
        catch (NullPointerException e) {
            // Here too
        }
        try {
            if (n.priority > n.right.priority) {
                return this.rotateLeft(n);
            }
        }
        catch (NullPointerException e) {
            // Don't like those nulls
        }

        return n;
    }

    // Insert given key and value into subtree rooted
    // at given node; return changed subtree with new
    // node added. Computes height and rebalances if
    // needed.
    private Node insert(Node n, K k, V v) {
        if (n == null) {
            return new Node(k, v);
        }
        if (k.compareTo(n.key) < 0) {
            n.left = this.insert(n.left, k, v);
        } else if (k.compareTo(n.key) > 0) {
            n.right = this.insert(n.right, k, v);
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }
        n = this.doBalance(n);
        return n;
    }

    @Override
    public void insert(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.insert(this.root, k, v);
        this.size += 1;
    }

    // Overloaded insert given key value, and priority into subtree rooted
    // at given node; return changed subtree with new
    // node added. Computes height and rebalances if
    // needed.
    private Node insert(Node n, K k, V v, int p) {
        if (n == null) {
            return new Node(k, v, p);
        }
        if (k.compareTo(n.key) < 0) {
            n.left = this.insert(n.left, k, v, p);
        } else if (k.compareTo(n.key) > 0) {
            n.right = this.insert(n.right, k, v, p);
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }
        n = this.doBalance(n);
        return n;
    }

    /**
     * Overloaded insert function.
     * @param k key to insert.
     * @param v value to insert.
     * @param p priority to insert.
     */
    public void insert(K k, V v, int p) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.insert(this.root, k, v, p);
        this.size += 1;
    }

    // Return node with maximum key in subtree rooted
    // at given node. (Iterative version because once
    // again recursion has no advantage here.)
    private Node max(Node n) {
        while (n.right != null) {
            n = n.right;
        }
        return n;
    }

    // Remove node with given key from subtree rooted at
    // given node; return changed subtree with given key
    // missing. Computes new height for each node and re-
    // balances if needed.
    private Node remove(Node n, K k) {
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        if (k.compareTo(n.key) < 0) {
            n.left = this.remove(n.left, k);
        } else if (k.compareTo(n.key) > 0) {
            n.right = this.remove(n.right, k);
        } else {
            n = this.remove(n);
        }
        if (n == null) {
            return n;
        }

        n = this.doBalance(n);

        return n;
    }

    // Remove given node and return the remaining tree.
    // Easy if the node has 0 or 1 child; if it has two
    // children, find the predecessor, copy its data to
    // the given node (thus removing the key we need to
    // get rid off), then remove the predecessor node.
    private Node remove(Node n) {
        // 0 and 1 child
        if (n.right == null) {
            return n.left;
        }
        if (n.left == null) {
            return n.right;
        }

        // 2 children
        Node max = this.max(n.left);
        n.key = max.key;
        n.value = max.value;
        n.priority = max.priority;
        n.left = this.remove(n.left, max.key);

        return n;
    }

    @Override
    public V remove(K k) {
        // Need this additional find() for the V return value, because the
        // private remove() method cannot return that in addition to the new
        // root. If we had been smarter and used a void return type, we would
        // not need to do this extra work.
        V v = this.findForSure(k).value;
        this.root = this.remove(this.root, k);
        this.size -= 1;
        return v;
    }

    // Recursively add keys from subtree rooted at given node
    // into the given list.
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) {
            return;
        }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }

    // If we don't have a StringBuilder yet, make one;
    // otherwise just reset it back to a clean slate.
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }

    // Recursively append string representations of keys and
    // values from subtree rooted at given node.
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) {
            return;
        }
        this.toStringHelper(n.left, s);
        s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");
        this.toStringHelper(n.right, s);
    }

    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{");

        this.toStringHelper(this.root, this.stringBuilder);

        int length = this.stringBuilder.length();
        if (length > 1) {
            // If anything was appended at all, get rid of
            // the last ", " the toStringHelper put in.
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}");

        return this.stringBuilder.toString();
    }

    /**
     * Function to give Breadth-first traversal.
     * @return string of traversal.
     */
    public String breadthFirst() {
        StringBuilder s = new StringBuilder();
        ArrayDeque<Node> q = new ArrayDeque<>();
        q.add(this.root);
        while (q.size() > 0) {
            Node n = q.removeFirst();
            s.append(n.key);
            s.append(": ");
            s.append(n.value);
            s.append(", ");
            //s.append("priority: ");
            //s.append(n.priority);
            //s.append(", ");
            if (n.left != null) {
                q.add(n.left);
            }
            if (n.right != null) {
                q.add(n.right);
            }
        }
        s.setLength(s.length() - 2);
        return s.toString();
    }

}
