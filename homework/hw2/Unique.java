/*
 * Alex Hepp
 * ahepp1
 * ahepp1@jhu.edu
 * Unique.java
 */

package hw2;

import java.util.Scanner;


/** Unique problem using a SparseArray and processing from standard in. */
public final class Unique {

    // make checkstyle happy
    private Unique() {}

    /**
     * Print only unique integers out of entered numbers.
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        Scanner sin = new Scanner(System.in);
        boolean found = false;
        int cap = 1;
        int size = 1;
        int num;
        // Create a simple array with default values of 0 and capacity of 1.
        SimpleArray<Integer> uniques = new SimpleArray<>(cap, 0);
        while (sin.hasNext()) {
            // If hasNext is true but hasNextInt is false, we know the next
            // input is not an integer, this we should throw an exception.
            if (!sin.hasNextInt()) {
                throw new IllegalArgumentException("Enter only integers!");
            }
            num = sin.nextInt();
            // Check to see if inputted number is already in out uniques array.
            for (int j: uniques) {
                if (num == j) {
                    found = true;
                }
            }
            // If adding to uniques array, check is capacity is too small and
            // adjust if needed. Otherwise just add the value in to the back
            // of the array.
            if (!found) {
                if (size >= cap) {
                    uniques = resize(uniques);
                    cap *= 2;
                }
                uniques.put(size - 1, num);
                size++;
            }
            found = false;
        }
        // Copy the uniques array (which might have redundant trailing 0s) to
        // a temporary array with the exact correct size.
        SimpleArray<Integer> temp = new SimpleArray<>(size - 1, 0);
        for (int i = 0; i < size - 1; i++) {
            temp.put(i, uniques.get(i));
        }
        // Print out the list of unique values.
//        for (Integer t: temp) {
////            System.out.println(t);
////        }
    }


    /**
     * Function to resize a simple array object. Doubles the size of the
     * original object.
     * @param arr - simple array to resize
     * @return a new resized array with the values of the parameter copied
     * into it.
     */
    private static SimpleArray<Integer> resize(
            SimpleArray<Integer> arr) {
        SimpleArray<Integer> temp = new SimpleArray<>(arr.length() * 2, 0);
        for (int i = 0; i < arr.length(); i++) {
            temp.put(i, arr.get(i));
        }
        return temp;
    }
}
