/* TODO - Add your name, JHED, and email.
 * PolyArray.java
 */

package hw2;

import exceptions.LengthException;
import exceptions.IndexException;
import java.util.ArrayList; // see note in main() below
import java.util.Iterator;


/**
 * Simple polymorphic test framework for arrays.
 * See last week's PolyCount. You need to add more test cases (meaning more
 * methods like testNewLength and testNewWrongLength below) to make sure all
 * preconditions and axioms are indeed as expected from the specification.
*/
public final class PolyArray {
    private static final int LENGTH = 113;
    private static final int INITIAL = 7;

    private PolyArray() {}

    // Function to test is length is correctly assigned the constructors.
    private static void testNewLength(Array<Integer> a) {
        assert a.length() == LENGTH;
    }

    // Function to test if exception is thrown when invalid length is passed
    // to each constructor
    private static void testNewWrongLength() {
        try {
            Array<Integer> a = new SimpleArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new ListArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SparseArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
    }

    // Function to test if exception is thrown in get if index is greater than
    // length or less than 0.
    private static void testWrongGet() {
        try {
            Array<Integer> a = new SimpleArray<>(2, INITIAL);
            a.get(3);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new ListArray<>(2, INITIAL);
            a.get(3);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SparseArray<>(2, INITIAL);
            a.get(3);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SimpleArray<>(2, INITIAL);
            a.get(-3);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new ListArray<>(2, INITIAL);
            a.get(-3);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SparseArray<>(2, INITIAL);
            a.get(-3);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
    }

    // Function to test if exception is thrown in put if index is greater than
    // length or less than 0.
    private static void testWrongPut() {
        try {
            Array<Integer> a = new SimpleArray<>(2, INITIAL);
            a.put(3, 1);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new ListArray<>(2, INITIAL);
            a.put(3, 1);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SparseArray<>(2, INITIAL);
            a.put(3, 1);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SimpleArray<>(2, INITIAL);
            a.put(-2, 4);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new ListArray<>(2, INITIAL);
            a.put(-2, 4);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SparseArray<>(2, INITIAL);
            a.put(-2, 4);
            assert false;
        } catch (IndexException e) {
            // passed the test, nothing to do
        }
    }

    // Function to test if each index returns the default value before any
    // puts() -- tests the get(new()) axiom.
    private static void testDefault(Array<Integer> a) {
        for (Integer i: a) {
            assert (i == INITIAL);
        }
    }

    // Function to test the get(put()) axiom.
    private static void testPutGet(Array<Integer> a) {
        assert a.get(0) == INITIAL;
        a.put(1, 5);
        assert a.get(1) == 5;
        a.put(1, INITIAL);
        assert a.get(1) == INITIAL;
        a.put(2, 23);
        assert a.get(2) == 23;
        a.put(2, 55);
        assert a.get(2) == 55;
    }

    /**
     * Run (mostly polymorphic) tests on various array implementations.
     * Make sure you run this with -enableassertions! We'll learn a much
     * better approach to unit testing later.
     *
     * @param args Command line arguments (ignored).
    */
    public static void main(String[] args) {
        // For various technical reasons, we cannot use a plain Java array here
        // like we did in PolyCount. Sorry.
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<>(LENGTH, INITIAL));
        arrays.add(new ListArray<>(LENGTH, INITIAL));
        arrays.add(new SparseArray<>(LENGTH, INITIAL));

        // Create a sparse array to test the iterator and toString function.
        SparseArray<Integer> temp = new SparseArray<>(12, 0);
        temp.put(2, 5);
        temp.put(5, 100);
        temp.put(3, 25);
        temp.put(2, 45);
        temp.put(1, 99);

        // Test my toString function
        assert (temp.toString().equals("[0, 99, 45, 25, 0, 100, 0, 0, 0, 0, " +
                "0, 0]"));

        // Test iterator
        StringBuilder s = new StringBuilder();
        for (Iterator it = temp.iterator(); it.hasNext();) {
            s.append(it.next());
            if (it.hasNext()) {
                s.append(" ");
            }
        }
        assert (s.toString().equals("0 99 45 25 0 100 0 0 0 0 0 0"));

        // Test all the axioms. We can do that nicely in a loop. In the test
        // methods, keep in mind that you are handed the same object over and
        // over again! I.e., order matters!
        for (Array<Integer> a: arrays) {
            testNewLength(a);
            testDefault(a);
            testPutGet(a);
        }

        // Exception testing. Sadly we have to code each one of these
        // out manually, not even Java's reflection API would help...
        testNewWrongLength();
        testWrongGet();
        testWrongPut();
    }
}
