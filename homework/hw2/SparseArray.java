/* TODO - Add your name, JHED, and email.
 * SparseArray.java
 */

package hw2;

import exceptions.IndexException;
import exceptions.LengthException;

import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * Implementation of an array using a linked list for sparse arrays. These
 * are arrays with mostly default values, making it efficient to only store
 * values that deviate from this default. Use this instead of a ListArray
 * when you have large data sets with the vast majority of the indices
 * remaining at a default value.
 * @param <T> Element type
 */
public class SparseArray<T> implements Array<T> {

    // A nested Node<T> class to build our linked list out of. We use a
    // nested class (instead of an inner class) here since we don't need
    // access to the ListArray object we're part of.
    private static class Node<T> {
        T data;
        Node<T> next;
        int pos;
    }

    // An inner iterator class to traverse the sparse array.
    private class SparseArrayIterator implements Iterator<T> {
        // Current position in the linked list.
        int current;

        // Initialize current to be at index 0;
        SparseArrayIterator() {
            this.current = 0;
        }

        // Check if there is a next value in the sparse array.
        @Override
        public boolean hasNext() {
            return this.current != SparseArray.this.length;
        }

        // Returns the next value in the sparse array.
        // @throws NoSuchElementException if the is not a next element in
        // sparse array.
        @Override
        public T next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            return SparseArray.this.get(current++);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    // Store a Node to the beginning of the list, a length so we dont have to
    // recompute length later and a default value to return if a node has not
    // been created for a position.
    private Node<T> list;
    private int length;
    private T defaultVal;

    /**
     * An array that is meant to be filled primarily with a default value
     * that is not going to change - with the benefit of that default
     * value not being stored numerous times as opposed to once.
     * @param length The number of indexes the array should have.
     * @param defaultValue The default value for the array.
     * @throws LengthException if length is less than 1.
    */
    public SparseArray(int length, T defaultValue) throws LengthException {
        if (length <= 0) {
            throw new LengthException();
        }
        this.defaultVal = defaultValue;
        this.length = length;
    }

    // Returns the stored length of the sparse array.
    @Override
    public int length() {
        return this.length;
    }

    // Method to get a value from an index.
    // @throws IndexException if index is less than 0 or greater than length
    // of the sparse array.
    @Override
    public T get(int i) throws IndexException {
        if (i < 0 || i >= this.length) {
            throw new IndexException();
        }
        Node<T> n = this.find(i);
        if (n != null) {
            return n.data;
        }
        else {
            return defaultVal;
        }
    }

    // Method to put a value into an index.
    // @throws IndexException if index is less than 0 or greater than length
    // of the sparse array.
    @Override
    public void put(int i, T t) throws IndexException {
        if (i < 0 || i >= this.length) {
            throw new IndexException();
        }
        Node<T> n = this.find(i);
        if (n != null) {
            n.data = t;
        }
        else {
            Node<T> temp = new Node<>();
            temp.data = t;
            temp.pos = i;
            temp.next = this.list;
            this.list = temp;
        }
    }

    // Find the node for a given index. Assumes valid index.
    private Node<T> find(int position) {
        Node<T> n = this.list;
        while (n != null) {
            if (n.pos == position) {
                return n;
            }
            n = n.next;
        }
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return new SparseArrayIterator();
    }


    //Decided to make a toString() function, iterates through the
    // sparse array and spits out default value if a node has not been
    // created, otherwise outputs the value put() into that position.
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("[");
        for (Iterator it = this.iterator(); it.hasNext();) {
            s.append(it.next());
            if (it.hasNext()) {
                s.append(", ");
            }
        }
        s.append("]");
        return s.toString();
    }
}
